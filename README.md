# **/Mini Projet AS2 - Threads**
> *Gaëtan GESTIN L3 II* - Projet Licence 3 Informatique, Architecture et Système 2 - Mini-projet système d'exploitation

## 0. Sommaire
- [**/Mini Projet AS2 - Threads**](#mini-projet-as2---threads)
  - [0. Sommaire](#0-sommaire)
  - [1. Description de la structure du programme](#1-description-de-la-structure-du-programme)
    - [Trame du code :](#trame-du-code-)
    - [Description du code :](#description-du-code-)
  - [2. Code du programme](#2-code-du-programme)
  - [3. Script de lancement du programme](#3-script-de-lancement-du-programme)
  - [4. Configuration materiele de la machine de tests](#4-configuration-materiele-de-la-machine-de-tests)
  - [5. Représentation graphique des résultats + Analyses](#5-représentation-graphique-des-résultats--analyses)
    - [**Méthode 1 Répartition par Composantes**](#méthode-1-répartition-par-composantes)
    - [**Méthode 2 Répartition par Blocs de Composantes**](#méthode-2-répartition-par-blocs-de-composantes)
    - [**Méthode 3 Répartition Dynamique par Blocs de Composantes**](#méthode-3-répartition-dynamique-par-blocs-de-composantes)
    - [**Moyenne temps par nombre de Threads Avec Migration**](#moyenne-temps-par-nombre-de-threads-avec-migration)
    - [**Moyenne temps par nombre de Threads Sans Migration**](#moyenne-temps-par-nombre-de-threads-sans-migration)

## 1. Description de la structure du programme
> La plupart des instructions sont commentées dans le code source. 😎

### Trame du code :
1. L'entrée `main(...);` reçoit 3 arguments (la méthode de répartition des threads, le nombre de threads et si il y migration) puis transmet tous ces arguments à la méthode `executeTests(...);`.
2. La méthode `executeTests(...);` ensuite appelle `NB_MESURE` fois la fonction de calcul des vecteurs `addition2Vector(...);` en transmétant aussi les arguments du `main(...);`. Cette fonction permet aussi de calculer le temps d'éxecution de la fonction afin de les exporter plus tard.
3. La méthode `addition2Vector(...);` fait le traitement en créant les threads selon les arguments dans une boucle créant `nbThreads` Threads avec la méthode appropriée (Méthode 1 -> `addition2ComposanteSimple(...);`), cette fonction attends ensuite la fin de tout les threads pour nettoyer les variables.
4. On affiche les résultats pour voir en direct la moyenne de calcul.
5. Et on exporte tout ces beaux résultats dans un fichier `resThreads.csv` qui est un fichier contenant `Migration coeurs, Méthode Repartition, Nombre Threads, Indice Mesure, Resultat Mesure` afin de pouvoir etre ouvert dans un tableur.

### Description du code :
- Le fichier `main.h` contient tout ce qu'un header doit contenir, les directives d'import ansi que les définitions pour les variables globales tel que :
  1. `NB_COMPOSANTES_VECTOR = 100000000` - le nombre de composantes dans les vecteurs.
  2. `NB_MESURE = 10` - le nombre d'iterations à effectuer pour les calculs du temps.
  3. `TAILLE_BLOC = 1000` - la taille d'un bloc de calcul.
  4. `typedef struct t_vector` - la structure représentant un vecteur avec `NB_COMPOSANTES_VECTOR` composantes.
  5. `typedef struct t_argAddition` - la structure que je passe en parametre à mes threads.

- Le fichier `main.c` contenant toute les affectations des méthodes :
  1. `main(...);` - trame principale du programme qui traite les arguments du programme, affiche les différents paramètres de la simulation, lance les tests de la simulation en stockant les temps dans un tableau qui est ensuite exporter en `.csv`.
  2. `executeTests(...);` - méthode qui permet d'appeler NB_MESURE fois une methode de repartition. Avec une sauvegarde du temps dans un tableau.
  3. `afficheResultat(...);` - méthode d'affichage simple de la moyenne des résultats.
  4. `addition2Vector(...);` - méthode qui va créer nbThreads en fonction de la methode de répartition des threads passé en parametre.
  5. `addition2ComposanteSimple(...);` - méthode de repartition Simple. Chaque Thread calcul une composante par une composante avec une repartition cyclique (0, 10, 20...).
  6. `addition2ComposanteBloc(...);` - méthode de repartition Bloc. Chaque Thread calcul plusieurs composantes au lieu d'une, avec une repartition cyclique (0->10, 10->20, 20->30).
  7. `addition2ComposanteFarming(...);` - méthode de repartition Farming. Chaque Thread demande dynamiquement quel bloc il doit calculer (pointeur partagé par tout les threads).
  8. `export2CSV(...);` - méthode d'export qui exporte le tableau de resultat dans un fichier `resThread.csv`.
  9. `affinity2Core(...);` - méthode permettant d'attacher un thread au core passe en parametre.

## 2. Code du programme
Tout le code peut etre télécharger ici : [lien GitLab](https://gitlab.com/GGestin/mini-projet-as2-threads).
</br>Ce dossier contient :
1. Un dossier `./exemple/` contenant un exemple de sortie de fichier `.csv`
2. Un dossier `./src/` contenant tout les fichiers sources
3. Un dossier `./documents` contenant tout les documents du project (sujet, graphiques rapports)
4. Un fichier `./launchPrg.sh` qui permet de lancer tout les tests du programmes et de sortir un fichier `.csv` contenant les résultats
5. Un fichier `./makefile` servant à la compilation du code

## 3. Script de lancement du programme
Le script de lancement du programme est un simple script qui suit ces étapes :
1. Suppression du fichier `resThread.csv` si il existe
2. Compilation du code pour creer un executable
3. Execute l'application pour chaque configuration possible (avec/sans migration, 3 méthodes de répartition, différent nombre de threads) pour un total de **66 tests**.
4. Affiche le temps total de l'éxecution.

## 4. Configuration materiele de la machine de tests
![ConfigurationMaterielle](documents/ConfigHardware.png)

## 5. Représentation graphique des résultats + Analyses
### **Méthode 1 Répartition par Composantes**
![Méthode1RépartitionParComposantes](documents/Méthode1RépartitionParComposantes.png)
> On remarque que la tendance de la courbe est croissante, ce qui est compréhensible à cause de la surcharge dûe à la création des threads, cependant on peut aussi remarquer une baisse du temps avec 1024 threads.

### **Méthode 2 Répartition par Blocs de Composantes**
![Méthode2RépartitionParBlocsDeComposantes](documents/Méthode2RépartitionParBlocsDeComposantes.png)
> Ce graphe est très constant le temps oscille entre 1.7s et 2.4s avec une croissance du temps liée aux nombre de threads,

### **Méthode 3 Répartition Dynamique par Blocs de Composantes**
![Méthode3RépartitionDynamiqueParBlocsDeComposantes](documents/Méthode3RépartitionDynamiqueParBlocsDeComposantes.png)
> La courbe de tendance de ce graphe est plutôt horizontale à partir de 4 Threads, le début étant plus ésotérique. On peut aussi noter une différence plus importante entre les temps avec ou sans migration, l'algorithme de répartition étant plus éfficace sans migration.

### **Moyenne temps par nombre de Threads Avec Migration**
![MoyenneDeSecondesParNombreDeThreadsAvecMigration](documents/MoyenneDeSecondesParNombreDeThreadsAvecMigration.png)
> On peut remarquer sans surprise que la répartition des threads par composantes (Méthode 1) n'est pas efficace à utiliser. Une remarque très intérresante que l'on peut faire est sur la moyenne de toute les méthodes de répartition avec 1 Thread, toute les méthodes ont le même temps car le programme est donc itératif et tout les algorithmes se valent à ce niveau. Sinon de manière générale la méthode de répartition des threads par blocs de composantes (Méthode 2) est légerement moins efficace que la méthode 3 car la méthode de Répartition Dynamique par Blocs de Composantes (Méthode 3) est plus "intelligente" car elle répartie mieux la charge de travail entre les threads. Cela est sans dout du a un certain cache CPU.

### **Moyenne temps par nombre de Threads Sans Migration**
![MoyenneDeSecondesParNombreDeThreadsSansMigration](documents/MoyenneDeSecondesParNombreDeThreadsSansMigration.png)
> Les remarques que l'on peut faire içi sont la même qu'au dessus, de plus les méthodes de répartition sont toute moins efficaces sauf pour la méthode 3 qui bénéficie légerement de cette répartition sans migration entre coeurs.