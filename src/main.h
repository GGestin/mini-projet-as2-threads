#define _GNU_SOURCE
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <string.h>
#include <sched.h>
#include <unistd.h>
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

// Nombre de composante dans un vecteur
#define NB_COMPOSANTES_VECTOR 100000000

// Nombre de mesures de tests a effectuer
#define NB_MESURE 10

// Taille d'un bloc de calcul (nombre de composantes)
#define TAILLE_BLOC 1000

/**
 * Structure d'un vecteur.
 * @param comp tableau de double representant les composantes d'un vecteur
 */
typedef struct t_vector
{
	double comp[NB_COMPOSANTES_VECTOR];
} vector;

/**
 * Structure pour passer des arguments à un thread
 * @param vectA Premier vecteur de l'addition
 * @param vectB Second vecteur de l'addition
 * @param vectC Vecteur stockant le resultat
 * @param iMin indice de la premiere composante de calcul
 * @param nbThreads nombre de threads du programme
 * @param nextBlock prochain bloc qui doit etre calculer (methode farming)
 */
typedef struct t_argAddition
{
	vector *vectA;
	vector *vectB;
	vector *vectC;
	int iMin;
	int nbThreads;
	int *nextBlock;
	int idCore;
	pthread_mutex_t zaWarudo;
} argAddition;

int main(int argc, char const *argv[]);
void executeTests(void (*f)(int, int, int), int method, int nbThreads, int migration, double *ret);
void afficheResultat(double *res);
void addition2Vector(int methode, int nbThreads, int migration);
void *addition2ComposanteSimple(void *args);
void *addition2ComposanteBloc(void *args);
void *addition2ComposanteFarming(void *args);
void export2CSV(int method, int nbThreads, int migration, double *res);
void affinity2Core(int core);