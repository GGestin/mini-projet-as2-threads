#include "main.h"

/**
 * Arg 1 : la methode de repartition des composantes
 * 			(0) : Repartition cyclique des composantes
 * 			 1  : Repartition cyclique des blocs de composantes
 * 			 2  : Repartition des blocs de composantes a la demande (farming)
 *
 * Arg 2 : le nombre de threads
 * 			(1024) : nombre de threads
 *
 * Arg 3 : l'autorisation de migration entre coeurs
 * 			(0) : non
 * 			 1  : oui
 **/
int main(int argc, char const *argv[])
{
	printf(ANSI_COLOR_CYAN "--> " ANSI_COLOR_GREEN "Lancement du Thread Principal\n" ANSI_COLOR_RESET);
	// Arg 1 : la methode de repartition des composantes
	int method;

	// Arg 2 : le nombre de threads
	int nbThreads;

	// Arg 3 : l'autorisation de migration entre coeurs
	int migration;

	// Verification des parametres
	if (argc != 4)
	{
		printf(ANSI_COLOR_RED "/!\\ Pas assez d'argument, passage en mode EZ (0 1024 0)\n" ANSI_COLOR_RESET);
		method = 0;
		nbThreads = 1024;
		migration = 0;
	}
	else
	{
		method = atoi(argv[1]);
		nbThreads = atoi(argv[2]);
		migration = atoi(argv[3]);
	}

	if (nbThreads > 1024 || nbThreads < 1)
	{
		printf(ANSI_COLOR_RED "Error : Nombre de threads doit etre 1 > nbThreads > 1024\nnbThreads: %d" ANSI_COLOR_RESET, nbThreads);
		exit(EXIT_FAILURE);
	}

	char *met = method == 0 ? ANSI_COLOR_BLUE "Repartition Cyclique par Composantes" ANSI_COLOR_RESET : method == 1 ? ANSI_COLOR_CYAN "Répartition Cyclique par Blocs de Composantes" ANSI_COLOR_RESET
																													: ANSI_COLOR_GREEN "Répartition Dynamique par Blocs de Composantes (farming)" ANSI_COLOR_RESET;
	printf("Parametres de la simulation : \n\
\t> " ANSI_COLOR_YELLOW "Methode de Thread:" ANSI_COLOR_RESET "\t\t\t%s\n\
\t> " ANSI_COLOR_YELLOW "Nombre de Threads:" ANSI_COLOR_RESET "\t\t\t%d\n\
\t> " ANSI_COLOR_YELLOW "Migration entre coeurs:" ANSI_COLOR_RESET "\t\t%s\n\
\t> " ANSI_COLOR_YELLOW "Nombre de composantes par vecteur:" ANSI_COLOR_RESET "\t%d\n\
\t> " ANSI_COLOR_YELLOW "Nombre de mesures par algorithme:" ANSI_COLOR_RESET "\t%d\n",
		met, nbThreads, migration == 0 ? ANSI_COLOR_RED "Non" ANSI_COLOR_RESET : ANSI_COLOR_GREEN "Oui" ANSI_COLOR_RESET, NB_COMPOSANTES_VECTOR, NB_MESURE);

	// Tableau de stockage des resultats
	double ret[NB_MESURE];
	executeTests(addition2Vector, method, nbThreads, migration, ret);
	afficheResultat(ret);
	export2CSV(method, nbThreads, migration, ret);
	return 0;
}

/**
 * Methode appeler pour lancer NB_MESURE fois une methode de repartition. Avec une sauvegarde du temps.
 * @param f la methode a tester (deprecated mais pour la beauté de l'art je le laisse)
 * @param method la methode de repartition
 * @param nbThreads le nombre de Threads du programme
 * @param ret le tableau des resultats
 */
void executeTests(void (*f)(int, int, int), int method, int nbThreads, int migration, double *ret)
{
	// Boucle pour mesurer NB_MESURE fois la methode f
	struct timeval start, end;
	for (int i = 0; i < NB_MESURE; i++)
	{
		gettimeofday(&start, NULL); // Get time actuel
		(*f)(method, nbThreads, migration); // Lance la fonction
		gettimeofday(&end, NULL); //Get time après
		ret[i] = (double)((end.tv_sec-start.tv_sec) * 1000000 + end.tv_usec-start.tv_usec) / 1000000;
		printf(ANSI_COLOR_CYAN "Mesure %d :" ANSI_COLOR_RESET " %0.3lfs\n", i + 1, ret[i]);
	}
}

/**
 * Simple affichage de la moyenne des resultat en parametre.
 * @param res le tableau de resultat
 */
void afficheResultat(double *res)
{
	double moy = 0;
	// Calcul de la moyenne
	for (int i = 0; i < NB_MESURE; i++)
		moy += res[i];
	printf("--> Resultat moyen : %.3lfs\n", moy / NB_MESURE);
}

/**
 * Methode qui va creer nbThreads en fonction de la methode en parametre.
 * @param method la methode de repartition des Threads
 * @param nbThreads le nombre de Threads du programme
 */
void addition2Vector(int method, int nbThreads, int migration)
{
	// Id du thread
	pthread_t threads[nbThreads];
	vector *vect1 = (vector *)malloc(sizeof(vector));
	vector *vect2 = (vector *)malloc(sizeof(vector));
	vector *vect3 = (vector *)malloc(sizeof(vector));

	// Setup des args dans la struct pour le passer en argument du pthread_create
	argAddition *args = (argAddition *)malloc(sizeof(argAddition));
	args->nbThreads = nbThreads;
	int p_nextBlock = 0;
	args->nextBlock = &p_nextBlock;
	args->vectA = vect1;
	args->vectB = vect2;
	args->vectC = vect3;

	// Initialiosation du mutex pour lock les zones critique
	pthread_mutex_t lock;
	pthread_mutex_init(&lock, NULL);
	args->zaWarudo = lock;

	// On obtient le nombre maximum de coeurs
	int maxCore = sysconf(_SC_NPROCESSORS_ONLN);

	// Boucle pour creer les threads
	for (int i = 0; i < nbThreads; i++)
	{
		// Setup de l'indice debut de calcul du thread
		args->iMin = i;

		// Si il y a pas migration idCore = -1 sinon au CPU à affecter
		args->idCore = migration ? i % maxCore : -1;

		// Selon la methode de repartition on change de fonction de calcul
		switch (method)
		{
		case 0:
			pthread_create(&threads[i], NULL, addition2ComposanteSimple, (void *)args);
			break;
		case 1:
			pthread_create(&threads[i], NULL, addition2ComposanteBloc, (void *)args);
			break;
		case 2:
			pthread_create(&threads[i], NULL, addition2ComposanteFarming, (void *)args);
			break;
		}
	}

	// Boucle permettant au main d'attendre que tout les Threads ont fini leur travail
	for (int i = 0; i < nbThreads; i++)
		pthread_join(threads[i], NULL);

	// Liberation des mallocs et lock
	pthread_mutex_destroy(&lock);
	free(vect1);
	free(vect2);
	free(vect3);
	free(args);
}

/**
 * Methode de repartition Simple.
 * Chaque Thread calcul une composante par une composante
 * avec une repartition cyclique (0, 10, 20)
 * @param arguments les arguments
 */
void *addition2ComposanteSimple(void *arguments)
{
	argAddition *args = (argAddition *)arguments;

	// On empeche la migration si idCore != -1
	if (args->idCore != -1)
		affinity2Core(args->idCore);

	for (int i = args->iMin; i < NB_COMPOSANTES_VECTOR; i += args->nbThreads)
		(args->vectC)->comp[i] = (args->vectA)->comp[i] + args->vectB->comp[i];
	pthread_exit(EXIT_SUCCESS);
}

/**
 * Methode de repartition Bloc.
 * Chaque Thread calcul plusieurs composantes au lieu d'une
 * avec une repartition cyclique (0->10, 10->20, 20->30)
 * @param arguments les arguments
 */
void *addition2ComposanteBloc(void *arguments)
{
	argAddition *args = (argAddition *)arguments;

	// On empeche la migration si idCore != -1
	if (args->idCore != -1)
		affinity2Core(args->idCore);

	// Premiere boucle permettant de parcourir toutes les composantes (avec un parcous cyclique)
	for (int i = args->iMin * TAILLE_BLOC; i < NB_COMPOSANTES_VECTOR - TAILLE_BLOC; i += args->nbThreads * TAILLE_BLOC)
		// Calcul du bloc (1000 composantes) par le thread
		for (int j = i; j < i + TAILLE_BLOC; j++){
			(args->vectC)->comp[j] = (args->vectA)->comp[j] + args->vectB->comp[j];
		}
	pthread_exit(EXIT_SUCCESS);
}

/**
 * Methode de repartition Farming.
 * Chaque Thread demande dynamiquement quel bloc il doit calculer
 * @param arguments les arguments
 */
void *addition2ComposanteFarming(void *arguments)
{
	argAddition *args = (argAddition *)arguments;

	// On empeche la migration si idCore != -1
	if (args->idCore != -1)
		affinity2Core(args->idCore);

	// Tant qu'il y a des calculs a faire
	while (*args->nextBlock < NB_COMPOSANTES_VECTOR - TAILLE_BLOC)
	{
		// Mis a jour du prochain bloc a calculer
		pthread_mutex_lock(&args->zaWarudo);
		int iBloc = *(args->nextBlock);
		*(args->nextBlock) += TAILLE_BLOC;
		pthread_mutex_unlock(&args->zaWarudo);

		// Calcul du bloc reserve par le thread
		for (int i = iBloc; i < iBloc + TAILLE_BLOC; i++)
			(args->vectC)->comp[i] = (args->vectA)->comp[i] + args->vectB->comp[i];
	}
	pthread_exit(EXIT_SUCCESS);
}

/**
 * Exporte le tableau de resultat dans un fichier "resThread.csv".
 * @param method la methode de calcul (0,1,2)
 * @param nbThreads le nombre de Threads (1-1024)
 * @param migration Autorisation de migration (0,1)
 * @param res le tableau contenant les resultats des calculs
 */
void export2CSV(int method, int nbThreads, int migration, double *res)
{
	// Ouverture du fichier "resThread.csv"
	char filename[16] = "resThread.csv";
	FILE *fp;
	// Setup des en-tetes du fichier csv
	if (fp = fopen(filename, "r"))
	{
		fclose(fp);
		fp = fopen(filename, "a+");
	}
	else // Si le fichier est inexistant
	{
		fp = fopen(filename, "a+");
		fprintf(fp, "Migration coeurs, Méthode Repartition, Nombre Threads, Indice Mesure, Resultat Mesure\n");
	}

	// Boucle pour ecrire a la fin du fichier les resultat
	for (int i = 0; i < NB_MESURE; i++)
		fprintf(fp, "%d, %d, %d, %d, %lf\n", migration, method, nbThreads, i, res[i]);
	fclose(fp);
}

/**
 * Fonction permettant d'attacher un thread au core passe en parametre.
 * @param core Le coeur a attacher le thread
 */
void affinity2Core(int core)
{
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(core, &cpuset);

	pthread_t this = pthread_self();
	pthread_setaffinity_np(this, sizeof(cpu_set_t), &cpuset);
}
