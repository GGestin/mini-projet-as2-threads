#!/bin/bash
RED='\033[0;31m'
CYAN='\033[0;36m'
NC='\033[0m'
SECONDS=0
CPT=0
NBBOUCLES=65

rm -f resThread.csv
make
for MIGRATION in 1 0
do
    for METHOD in 0 1 2
    do
        for NBTHREADS in 1 2 4 8 16 32 64 128 256 512 1024
        do
        clear
        echo -e "=>>> ${RED}$((($CPT*100/$NBBOUCLES*100)/100))${NC} % done (${CYAN}$CPT${NC}/${RED}$NBBOUCLES${NC})"
        ./exe/main $METHOD $NBTHREADS $MIGRATION
        CPT=$(($CPT+1))
        done
    done
done
DURATION=$SECONDS
echo -e "${RED}$(($DURATION / 60))m$(($DURATION % 60))s de calculs.${NC}"