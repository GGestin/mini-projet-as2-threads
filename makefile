build: exe
	gcc -c src/main.c -o bin/main.o -lpthread -g
	gcc bin/main.o -o exe/main -lpthread -g

exec: build
	./launchPrg.sh

exe:
	mkdir exe bin

clean:
	rm -rf bin exe